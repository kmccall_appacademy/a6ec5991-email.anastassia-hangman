#Anastassia Bobokalonova

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players = {})
    @players = players
    @guesser = @players[:guesser]
    @referee = @players[:referee]
    @board = []
  end

  def setup
    @length = @referee.pick_secret_word
    @guesser.register_secret_length(@length)
    @board = Array.new(@length)
    self.display
  end

  def take_turn
    @guess = @guesser.guess
    @indices = @referee.check_guess(@guess)
    self.update_board(@indices)
    @guesser.handle_response
  end

  def update_board(indices)
    indices.each do |i|
      @board[i] = @guess
    end
    self.display
  end

  def display
    @board.each do |el|
      el == nil ? (print "_ ") : (print "#{el} ")
    end
    puts
  end

  def play
    self.setup
    until over?
      self.take_turn
    end
    puts "Congratulations, you guessed the word!"
  end

  def over?
    board.none? {|el| el == nil}
  end

end

class HumanPlayer

  def initialize(name)
    @name = name
  end

  def pick_secret_word
    puts "#{@name}, whats the length of your word? "
    @length = gets.chomp
    register_secret_length(@length)
  end

  def check_guess

  end

  def register_secret_length(length)
    @length = length
  end

  def guess
    puts "#{@name}, guess a letter: "
    @guess = gets.chomp
  end

  def handle_response

  end

end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary
    #@dictionary = File.open('dictionary.txt')
  end

  def pick_secret_word
    @word = @dictionary.shuffle[0]
    @word.length
  end

  def check_guess(letter)
    indices = []
    @word.chars.each_with_index do |ch, i|
      indices << i if ch == letter
    end
    indices
  end

  def register_secret_length(length)
    @length = length
  end

  def guess
    puts "ComputerPlayer, make your guess: "
  end

  def handle_response

  end

end

puts "===================\nWelcome to Hangman!\n==================="
puts "What is your name?"
name = gets.chomp

human = HumanPlayer.new(name)
computer = ComputerPlayer.new(["sample", "words"])
game = Hangman.new(guesser: human, referee: computer)
game.play
